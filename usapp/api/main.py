from flask import Flask
from waitress import serve
from flaskext.mysql import MySQL
import json
import os

app = Flask(__name__)
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = os.environ.get('DB_USER')
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get('DB_PASSWORD')
app.config['MYSQL_DATABASE_DB'] = os.environ.get('DB_NAME')
app.config['MYSQL_DATABASE_HOST'] = os.environ.get('DB_HOST')
mysql.init_app(app)

@app.route('/')
def main_page():
    return 'Main Page!'

@app.route('/hello')
def hello_world():
    return 'Hello World!'


@app.route('/user/')
def users():
    query = "select id,firstname,lastname,email,reg_date from users"
    try:
        conn = mysql.connect()
        cursor =conn.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
    except Exception as e:
        return "Execption in database: " + str(e)
    response = {}
    for row in data:
        response[row[0]] = {"first_name":str(row[1]),"last_name":str(row[2]),"email":str(row[3]),"reg_date":str(row[4])}

    if not response:
        response = {"message":"record not found"}
    response = json.dumps(response)
    return response


if __name__ == '__main__':
    if os.environ.get('APP_ENV') == "production":
        serve(app, host="0.0.0.0", port=8081)
    else:
        app.run(host='0.0.0.0', port=8080)