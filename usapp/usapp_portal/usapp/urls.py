from django.urls import path
from . import views

urlpatterns = [
    path('', views.mainpage,name='index'),
    path('hello',views.hello,name="hello"),
    path('users',views.users,name="users"),

]