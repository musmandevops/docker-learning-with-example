from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import os

# Create your views here.
def mainpage(request):
    return HttpResponse("This is main page!")

def hello(request):
    return HttpResponse("Hello World!")

def users(request):
    url = os.environ.get('API_URL') + "/users"
    header = {
    "Content-Type":"application/json"
    }
    
    result = requests.get(url,headers=header)

    if result.status_code == 200:
        result_json = result.json()
        for row in result_json.values():
#            if row["first_name"] == "usman":
            return render(request, "users.html", row
            )
    return HttpResponse('Something went wrong')